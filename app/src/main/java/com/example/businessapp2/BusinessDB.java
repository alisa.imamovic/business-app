package com.example.businessapp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class BusinessDB {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "business_title";
    public static final String KEY_DESCRIPTION = "business_description";
    public static final String KEY_PHONE_NUMBER = "business_phone_number";

    private final  String DATABASE_NAME = "BusinessesDB";
    private final String DATABASE_TABLE = "BusinessesTable";
    private final int DATABASE_VERSION = 1;

    private DBHelper ourHelper;
    private final Context ourContext;
    private  SQLiteDatabase ourDatabase;

    public BusinessDB(Context context)
    {
        ourContext = context;
    }

    public class DBHelper extends SQLiteOpenHelper {
        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // poziva se kada baza nije prethodno kreirana, znaci samo prvi put, kreira bazu sa zadanim tabelama i verzijom
            // ako ne baci exception, znaci da se uspjesno kreirala baza sa zadanom verzijom

            /* Ovo je kako bi izgledalo u standardnoj bazi:

                CREATE TABLE BusinessesTable(_id INTEGER KEY AUTOINCREMENT,
                                            business_title TEXT NOT NULL,
                                            business_description TEXT NOT NULL,
                                            business_phone_number INTEGER NOT NULL);
                Ali u Javi se ovako pise:
             */



            String sqlCode = "CREATE TABLE " + DATABASE_TABLE + " (" +
                    KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    KEY_TITLE + " TEXT NOT NULL, " +
                    KEY_DESCRIPTION + " TEXT NOT NULL, " +
                    KEY_PHONE_NUMBER + " TEXT NOT NULL);";

            db.execSQL(sqlCode);    // kazemo da se izvrsi ovaj sqlCode koji smo napisali, nad nasom bazom
            // kad se to izvrsi imat cemo tu tabelu

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // poziva se kada baza vec postoji ali je DATABASE_VERSION drugaciji od trenutnog

            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);   // kad upgradeujemo verziju baze, dropamo staru tabelu (obrisemo je skroz) i trebamo kreirati novu

            onCreate(db);
        }
    }

    public BusinessDB open() throws SQLException
    {
        // ovaj metod nam sluzi da "otvorimo" bazu, tj da mozemo od sad u nju pisat ili citat iz nje
        // inicijaliziramo ourHelper

        ourHelper = new DBHelper(ourContext);
        ourDatabase = ourHelper.getWritableDatabase();
        return this;
    }

    public void close()
    {
        ourHelper.close();
    }


    public long createEntry(String title, String description, String phoneNum)
    {
        ContentValues cv = new ContentValues();
        cv.put(KEY_TITLE, title); // put metod uzima key i value, key nam je naziv kolone tj atributa a value vrijednost koju hocemo da ubacimo
        cv.put(KEY_DESCRIPTION, description);
        cv.put(KEY_PHONE_NUMBER, phoneNum);

        return ourDatabase.insert(DATABASE_TABLE, null, cv);
        // insert metod vraca broj elemenata koje smo ubacili u bazu
        // zato koristimo long jer mozemo puno elemenata ubacivati, a sada ubacujemo samo 1
    }


    // citanje iz baze
    public void getData(ArrayList<Business> lista)
    {
        if(!lista.isEmpty())
            lista.clear();

        String[] columns = new String[]{KEY_ROWID, KEY_TITLE, KEY_DESCRIPTION, KEY_PHONE_NUMBER};

        Cursor c = ourDatabase.query(DATABASE_TABLE, columns, null, null, null, null, null);


        int iRowID = c.getColumnIndex(KEY_ROWID); // mozda nam i ne treba
        int iTitle = c.getColumnIndex(KEY_TITLE);
        int iDescription = c.getColumnIndex(KEY_DESCRIPTION);
        int iPhoneNum = c.getColumnIndex(KEY_PHONE_NUMBER);

        for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
        {
            lista.add(new Business(c.getString(iTitle), c.getString(iDescription), c.getString(iPhoneNum)));
        }

        c.close();

    }

}
