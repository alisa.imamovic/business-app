package com.example.businessapp2;

public class Business {

       // String id;
        String title;
        String description;
        String phoneNumber;


        public Business(String title, String description, String phoneNumber) {
           // this.id = ID;
            this.title = title;
            this.description = description;
            this.phoneNumber = phoneNumber;
        }

       // public String getId() { return id;}

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

       // public void setId(String ID) { this.id = ID;}

        public void setTitle(String title) {
            this.title = title;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }

