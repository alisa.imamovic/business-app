package com.example.businessapp2;

import android.app.Application;
import android.database.SQLException;
import android.widget.Toast;

import java.util.ArrayList;

public class StartClass extends Application {

    public static ArrayList<Business> businesses;
    public static BusinessDB myDB;

    @Override
    public void onCreate() {
        super.onCreate();

        try
        {
            myDB = new BusinessDB(this);
            myDB.open();
            businesses = new ArrayList<>();
            myDB.getData(businesses);
            myDB.close();
        }
        catch(SQLException e)
        {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }


    }
}
