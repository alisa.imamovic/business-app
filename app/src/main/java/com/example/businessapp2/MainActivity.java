package com.example.businessapp2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements MyAdapter.ItemClicked {

    TextView titleTV, descriptionTV, cellTV; // ono sto ispisujemo

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titleTV = findViewById(R.id.title_b);
        descriptionTV = findViewById(R.id.description_b);
        cellTV = findViewById(R.id.cell_b);

        FloatingActionButton buttonAdd = findViewById(R.id.add_new);

        FragmentManager manager = this.getSupportFragmentManager();

        manager.beginTransaction()
                .hide(manager.findFragmentById(R.id.fragDet))
                .hide(manager.findFragmentById(R.id.fragAdd))
                .show(manager.findFragmentById(R.id.fragList))
                .addToBackStack(null)
                .commit();

        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addItem();
            }
        });

     }


    private void addItem()
    {
        FragmentManager manager = this.getSupportFragmentManager();

        manager.beginTransaction()
                .hide(manager.findFragmentById(R.id.fragDet))
                .show(manager.findFragmentById(R.id.fragAdd))
                .hide(manager.findFragmentById(R.id.fragList))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onItemClicked(int index) {

        titleTV.setText(StartClass.businesses.get(index).getTitle());
        descriptionTV.setText(StartClass.businesses.get(index).getDescription());
        cellTV.setText(StartClass.businesses.get(index).getPhoneNumber());

        FragmentManager manager = this.getSupportFragmentManager();
        manager.beginTransaction()
                .hide(manager.findFragmentById(R.id.fragAdd))
                .show(manager.findFragmentById(R.id.fragDet))
                .hide(manager.findFragmentById(R.id.fragList))
                .addToBackStack(null)
                .commit();

    }


}
