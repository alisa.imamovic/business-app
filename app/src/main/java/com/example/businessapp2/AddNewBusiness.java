package com.example.businessapp2;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddNewBusiness extends Fragment {

    EditText mTitle, mDescription,mCell; // polja koja unosimo
    Button saveB;
    View v;
    RecyclerView recView;


    public AddNewBusiness() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v =  inflater.inflate(R.layout.fragment_add_new_business, container, false);
        return v;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mTitle = v.findViewById(R.id.etTitle);
        mDescription = v.findViewById(R.id.etDescription);
        mCell = v.findViewById(R.id.etPhoneNumber);

        saveB = v.findViewById(R.id.saveButton);

        recView = getView().findViewById(R.id.recyclerview);

        saveB.setOnClickListener((v)-> {

            if(mTitle.getText().toString().trim().length() == 0 || mDescription.getText().toString().trim().length() == 0 || mCell.getText().toString().trim().isEmpty() == true)
            {
                Toast.makeText(getActivity(),"Please enter all fields!", Toast.LENGTH_SHORT).show();
                return;
            }


            String title = mTitle.getText().toString().trim();
            String descr = mDescription.getText().toString().trim();
            String cellPhone = mCell.getText().toString().trim();

            StartClass.myDB.open();
            StartClass.myDB.createEntry(title, descr, cellPhone);
            StartClass.myDB.getData(StartClass.businesses);
            StartClass.myDB.close();

            FragmentManager manager = getActivity().getSupportFragmentManager();

            ListFragment lFrag = (ListFragment) manager.findFragmentById(R.id.fragList);
            lFrag.update();

            manager.beginTransaction()
                    .hide(manager.findFragmentById(R.id.fragDet))
                    .hide(manager.findFragmentById(R.id.fragAdd))
                    .show(manager.findFragmentById(R.id.fragList))
                    .commit();


            Toast.makeText(getActivity(),"Sucessfully added!", Toast.LENGTH_SHORT).show();

            mTitle.getText().clear();
            mDescription.getText().clear();
            mCell.getText().clear();

        });
    }


}

