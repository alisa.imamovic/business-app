package com.example.businessapp2;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{

    private ArrayList<Business> businesses;
    ItemClicked activity;

    public interface ItemClicked
    {
        void onItemClicked(int index);
    }

    public MyAdapter(Context context, ArrayList<Business> b)
    {
       activity = (ItemClicked)context;
       this.businesses = b;

    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
         TextView titleB, descriptionB;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            titleB = itemView.findViewById(R.id.businessTitle);
            descriptionB = itemView.findViewById(R.id.businessDescription);


            itemView.setOnClickListener((v) -> {
                activity.onItemClicked(businesses.indexOf((Business)v.getTag()));
                    }
            );
        }
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.app_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapter.ViewHolder holder, int position) {

        holder.itemView.setTag(businesses.get(position));
        holder.titleB.setText(businesses.get(position).getTitle());
        holder.descriptionB.setText(businesses.get(position).getDescription());

        }

    @Override
    public int getItemCount() {
        return businesses.size();
    }

}
